
Project Review
=====================================================

## Overview

1) Briefly summarize the experiment in this project.

The experiemnt studies the performance of SDN controllers POX and RYU by measuring their latency and flows per second using a
benchmarking tool cbench that emulates required number of switches (4,8,16,32) and 1000 MAC's per switches.

3 slices were created, each with one VM, running cbench tool, POX and RYU controller.Cbench emulates the requirements and gives the
performance output.
The performance was studied over different number of switches (4,8,16,32) and 1000 hosts per switch.
The latency was measured using 1 switch and 10^5 MAC's per host.



2) Does the project generally follow the guidelines and parameters we have
learned in class?

Yes, to an extent, since the project has a broad goal of studying the performance of 2 SDN Controllers, POX and RYU by using metric : latency and
flows per second, and varying parameters: no. of switches and MAC's per host. The project should have included more details, because performance being
a broad topic, it must have included more metrics and conditions to be checked.
But, the project report was nicely written and easy to follow and minimal manual steps had to be folowed to complete the reproducibilty of the experiment.
The results obtained were analysed and represented in a graphical manner and also using tables .


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

Main aim of the project is to study the performanc of SDN controllers POX and RYU by measuring their latency and flows per second using a benchmarking tool
cbench that emulates required number of switches and MAC's per switches.
Now, the goal is focussed, but not a specific one, since study the performance of SDN controllers, being a braod topic, can have more conditions to be checked.

Hence a specific goal can be : to measure the latency and flows per second of the SDN controllers POX and RYU, by varying number of switches
and MAC's per host using cbench tool.


2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

Yes, the metrics : latency and flows per second, and parameters: number of switches and MAC's per host , appropriate for the experiment goal.
Since,POX and RYU controllers are python based that do not support multi-threading. Hence, their performance does not depend on the number of switches(as written in project report)
So, we may have omitted taking no. of switches as parameter, since the performance does not depend on it.
Again, there could have been more metrics or conditions to be checked for performance evaluation of SDN controllers.

Yes, the experiment design clearly support the experiment goal.In the experiement design, there are 3 slices , each for POX, RYU controller and
Cbench tool.Latency and flows per second is measured by sending data from cbench to the IP Addresses of both the controllers one by one. And then
the results are communicated using tables and graphs.


3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

Yes, the experiment is well designed.
But, to evaluate a broad topic of performanc evaluation, more trials could have been taken by varying flows per second, and that
could add to the complexity of the experiment.


4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

Yes, they are clear and unambiguous. But, since the performance of POX and RYU controller does not depend on the no. of switches, so maybe this metric could have been only used to measure latency.
There can be other metrics as well, but in reality system wide performance is likely a complex function of topology, work load, equipment, and forwarding complexity.
Single-controller microbenchmarks presented in this project is just a first step towards the understanding of the performance implications of SDN.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

The parameters: no. of switches and MAC's per host are meaningful. As varying these would give us the result for throughput and latency .
But again, no. of switches may have been ommited as parameter for measuring the throughput, since the performance of POX and RYU does not depend on it.


6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

Yes, the authors have sufficiently addressed the possibility of interactions between parameters. Actually, by varying number of switches,
throughput is obtained, and latency is given by varying the MAC's per host (10^5) and keeping no. of switch fixed to one.



7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

Yes, the comprisions are reasonable.
But in reality, performance of an SDN/OpenFlow controller is defined by two characteristics: throughput and latency.
The changes in the parameters when adding more switches and hosts to the network show the scalability of the controller.


## Communicating results


1) Do the authors report the quantitative results of their experiment?

Yes, they did report the quantative results in a tabular form in the project report.


2) Is there information given about the variation and/or distribution of
experimental results?

Yes, the information is given about the experimental results through graphs and table.
And, the graphs are clearly defined and made.

Rstudio login information was not there in there report, but that's a minor issue.


3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

Yes, the authors practice data integrity, there seems to be no practices to artificially making the results better.
As, all the graphs for comparing the data are very clearly defined and presented.


4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

I think there should have been line chart for the graph for different no. of switches to make it more specific and presentable to show
that the performance does not depend on no. of switches.



5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

Explicit conclusions are not made in the report, but the results are nicely communicated using graphs and tables.
By seeing the results, one can say that RYU has better throughput (flows/second), when no. of switches are varied. And, POX performs better when no. of MAC's per switch is varied.



## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

The authors included instructions for reproducing the experiment.
But , I faced problems, while setting up the experiment, majorly cbench (error in cbench command).
Also, more R commands were required to get the result, but they were simple commands.


2) Were you able to successfully produce experiment results?

Yes, I was able to produce the experiment results.

There were similar graphs for flows per second with different no. of switches and different no. of MAC's per switch.

Just there were difference in the latency values by me and the authors,

Latency :
              ME                AUTHORS' REPORT
RYU :   0.4471 ms/flow          0.5164 ms/flow
POX :   0.4364 ms/flow          0.6554 ms/flow



3) How long did it take you to run this experiment, from start to finish?

It took around 20 hours for me to run this experiment from start to finish.
I had to wait for the reservation of slice resources for cbench, and cbench execution. Hence, major time was spent on it.


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

Yes, extra steps were required during the setting up of cbench. Since there was a mistake in the cbench command in the report.


5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

I would characterize this experiment in the 3 (third degree of reproducibility). Since, the results can be reproduced by an independent researcher, requiring considerable effort.

## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

The project seemed to be tough, but the authors did it very systematically.
The graphs and tables, specifying the results were given clearly in the report.

I would appreciate if the report would have clear and explicit conclusions regarding the better performance of the controllers, written in the report.
