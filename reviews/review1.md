
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.
This experiment involves a quantification of performance of the SDN controllers POX and RYU. The experiment measures latency and flows per second. Since POX and RYU do not support multi-threading, the claim is that the performance of the controllers does not depend on the number of switches. To prove this idea, for each controller, the switches are configured at 4, 8, 16, and 32 each with 1000 MAC per switch by running the experiment four times.

2) Does the project generally follow the guidelines and parameters we have learned in class?
In my opinion this experiment follows the guidelines taught in class. First, the experiment is easy to follow and it is clear when performing the experiment why particular actions are taken and why the chosen metrics are used. There is no bias in testing the two controllers because it was not a system created by the students. In addition, the two controllers were treated exactly in the same way when the parameters were applied. Therefore, the results in the figures show objective results based on equivalent manipulation of the controllers. Finally, the experimenters are testing a network theory that may or may not be perfectly supported in the physical world rather than the theoretical.

## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal? Is it useful and likely to have interesting results?
The goal of this experiment is to compare the performance of two SDN controllers. In addition, the theory of adding MAC hosts without disrupting the performance of the controllers. I think that the goal is specific because all the parameter and metrics are well defined and clear. Since the design asks for particular metrics to define a strong or poor result. The results are not exceptionally interesting because this this type of experimentation has been done and there are papers describing which controllers on the market are best.
http://sit.sit.fraunhofer.de/mne/publications/download/SDNControllers.pdf

2) Are the metric(s) and parameters chosen appropriate for this experiment goal? Does the experiment design clearly support the experiment goal?
The parameters are the two controllers (POX and RYU) and the number of switches on each controller. The metrics are the latency and the flows per second. The parameters are well chosen to diversify the conditions of the experiment to accommodate the defined goal. For the metrics, performance is often measured by latency refers to the amount of time it takes for data to travel across a network. Measuring flows per second is also a good parameter since it may be used to calculate the bandwidth to get a complete picture of how much data is being transferred.

3) Is the experiment well designed to obtain maximum information with the minimum number of trials?
By running the described trials it is possible to see a trend without an extensive number of key strokes.

4) Are the metrics selected for study the right metrics? Are they clear, unambiguous, and likely to lead to correct conclusions? Are there other metrics that might have been better suited for this experiment?
The metrics chosen to determine performance are characteristic of a typical network. Latency and flows per second show a prognosis of the channel and are well chosen to evaluate as results.

5) Are the parameters of the experiment meaningful? Are the ranges over which parameters vary meaningful and representative?
I believe that the parameter of the number of switches is more significant than the controllers. The results of comparable performance have been done by researchers previously and the experimental information is readily available. The performance under the condition of operating with many switches is more interesting since it is a software defined network it should match the physical results with high performance.

6) Have the authors sufficiently addressed the possibility of interactions between parameters?
The author has designed the experiment such that the parameters do not interfere with one another.

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate and realistic?
Perhaps it would have been productive to start the baseline of connecting the controller to only one switch to identify the point to point communication between the two. However, comparing the performance of a various number of switches to each other provided results but without a margin of error.

## Communicating results

1) Do the authors report the quantitative results of their experiment?
Yes, the report shows raw data as well as multiple figures that show comparative results. The authors show a sample of the results that were generated when the experiment was primarily conducted.
2) Is there information given about the variation and/or distribution of experimental results?
The distribution of the results was not calculated and such results were not required when I ran the experiment. They would have been very useful here because the authors claim that the performance should not vary with additional switches. It would have seen useful to be the degree of error for the system as the number of switches changes.

3) Do the authors practice data integrity - telling the truth about their data, avoiding ratio games and other practices to artificially make their results seem better?
The data is presented in an unbiased way. There are no ratios and the representation is raw so that the figures present only that was generated by the measurement tools.

4) Is the data presented in a clear and effective way? If the data is presented in graphical form, is the type of graph selected appropriate for the "story" that the data is telling?
The graphic is well chosen to represent the results of the experiment. The line graph method shows the trend well across the change of the number of switches attached to the controller.

5) Are the conclusions drawn by the authors sufficiently supported by the experiment results?
The integrity of the data allows for an accurate show of results which means that the authors can be trusted and there is a conclusive fact to be evaluated at the end of the experiment. The conclusion of the experiment is therefore supported by the provided data.

## Reproducible research

1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results, Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear and easy to understand?
The authors provided all the various set-ups for the experiment. The instructions were very clear and logical and allowed me to complete the experiment within a short amount of time while retrieving a significant amount of data.

2) Were you able to successfully produce experiment results?
Yes, I was able to successfully produce results which were comparable to the designers.

3) How long did it take you to run this experiment, from start to finish?
Form the time I installed the first controller to the time I plotted the results on R, it took me about 13 minutes to complete the experiment.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe in detail. How long did these extra steps or changes take to figure out?
All the instructions were very clear. I spent about six minutes generating results and the rest of the time generating figures. The only extra steps that were taken was to run the command to alter the number of switches and hosts for each trial of the experiment;

5) In the lecture on reproducible experiments, we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
This experiment was a level 5 on the six degrees of reproducibility. All the tools needed for this experiment were readily available and it did not take more than 15 minutes to complete. The instructions were clear and concise and fulfilled the needs of the goal.  
