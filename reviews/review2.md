
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

The goal of the experiment is to study the performance of SDN controllers
POX and RYU by measuring the latency and flows per second using a benchmarking
tool called cbench. It is performed using three slices, one with a node
running the cbench tool, one for POX controller and one for the RYU controller.
After reserving resources, one must log in to the three VMs and install
necessary tools/softwares. Then the flows per second and the latency data
are generated.

2) Does the project generally follow the guidelines and parameters we have
learned in class?

The project follows the guidelines and parameters we have learned in class.
The goal of the experiment is unbiased and clear. The experiment is designed
to meet the goal.


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

The goal of the experiment is to study the performance of SDN controllers
POX and RYU my measuring the latency and flows per second using a benchmarking
tool called cbench. The goal is focused and clear. It is useful as it
compares two competing SDN frameworks and tries to find out which one is better.
It will be interesting to know which one performs better.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

The metrics and parameters are appropriate or the experiment goal. They have chosen
to vary the the number of switches keeping the number of hosts (MACs) the same (case 1)
and then the number of hosts keeping the number of switches the same (case 2).
They are measuring the latency and flows per second which gives a good idea of
the performance of the controller.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

The experiment is well designed and it does provide maximum information with less efforts.
They have used two metrics instead of one and one may ask whether it was necessary.
According to me, it was necessary as the latency or flows per second alone cannot
objectively determine which controller performs better.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

The two metrics selected are the right metrics. They are clear and unambiguous
and are likely to lead to correct conclusions. I would have selected the same metrics.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

The parameters of the experiment are meaningful. The range of the parameters
is enough to represent the sizes of typical SDNs.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

The authors have not mentioned anything about the possibility of interactions.

7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

The comparison that they have decided to make is reasonable. The parameters and
the metrics that they have selected can provide a good basis for comparison.

## Communicating results


1) Do the authors report the quantitative results of their experiment?

The authors have reported the quantitative results of their experiments by summarizing
the important data in neat tables.

2) Is there information given about the variation and/or distribution of
experimental results?

They have given the range of the parameters but no information is provided about
the variation or distribution of the experimental results.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

The authors have provided their data with integrity. For improvement, the latency data
too can be taken in the same format as the flows per second (with two cases and several trials).
With only one trial and fixed parameters we cannot judge the performance of the two controllers.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

The data they have presented is very clear. They have presented neat graphs along with the
data and they are appropriate for this type of data.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

The authors have not made any conclusion.

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

The authors have provided instructions for reproducing the experiment, but only
in two ways: Set up experiment and Existing experiment setup -> Data.
The instructions are clear and easy to follow. But even after repeated trials
some of the commands (related to installation of cbench) do not work. I read their
references and it had no mention of those commands. I searched for a solution online but
I couldn’t find anything. I then moved on to running the experiment using existing experiment setup.

2) Were you able to successfully produce experiment results?

I was not able to login to the VMs of the authors’ experimental setup.

3) How long did it take you to run this experiment, from start to finish?

It took me more than 5 hours but I still could not reproduce the experiment.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

As I mentioned before, to troubleshoot the errors during cbench installation I tried to find a solution
in the references mentioned by the authors. When that didn’t work I tried to search online for a
solution but I did not find anything. I spent approximately four hours on it.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

I would rate it as having a degree 3 of reproducibility.

## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.


The experiment was well designed and the efforts you have taken in representing the data and
making graphs can be seen clearly. There may have been some typos or you may have missed providing s
ome information which would have made reproducing the experiment easier. It would have greatly
helped if you would have released the rspec for each of the slice along with the report. Overall,
it is an interesting project. I enjoyed working on it and I learnt a lot about SDN controllers.
